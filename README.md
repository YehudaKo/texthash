# README #
This quick example for hashing string in java with java standard libs 

### Copyrights ###
Copyrights (c) 2017 Matrix BI LTD to Present
All rights reserved.
 
### How do I get set up? ###
No maven packages required to install except TestNG for testing purpeses 
you can make `mvn install` and see results

In test folder there is test code example search for file `HasherTest.java`

