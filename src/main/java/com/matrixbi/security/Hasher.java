/********************************************************************
 * 
 * Copyrights (c) 2017 Matrix BI LTD to Present
 * All rights reserved.
 * 
 * @author Yehuda Korotkin <yehudako@matrixbi.co.il>
 *  
 ********************************************************************/
package com.matrixbi.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
/**
 * Hasher helper class 
 * @author Yehuda Korotkin <yehudako@matrixbi.co.il>
 *
 */
public class Hasher {
	/**
	 * Instances
	 */
	private static HashMap<String, MessageDigest> instances = new HashMap<String, MessageDigest>();
	/**
	 * Singleton impl
	 * @param type
	 * @return
	 */
	private static MessageDigest getInstance(String type) {
		if (instances.containsKey(type))
			return instances.get(type);

		MessageDigest md = null;

		try {
			md = MessageDigest.getInstance(type);
			instances.put(type, md);
		} catch (NoSuchAlgorithmException e) {
			System.err.println("NoSuchAlgorithmException: " + type);
			e.printStackTrace();	
		}
		return md;
	}
	/**
	 * Converts bytes to string
	 * @param digest
	 * @return
	 */
	private static String toHexString(byte[] digest) {
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}

		return sb.toString();
	}
	/**
	 * to
	 * 
	 * MessageDigest Algorithms:
	 * MD2 		- The MD2 message digest algorithm as defined in RFC 1319.
	 * MD5 		- The MD5 message digest algorithm as defined in RFC 1321.
	 * SHA-1	- Hash algorithms defined in the FIPS PUB 180-4.
	 * SHA-224	 
	 * SHA-256
 	 * SHA-384
	 * SHA-512
	 * 
	 *  Secure hash algorithms - SHA-1, SHA-224, SHA-256, SHA-384, SHA-512 - for computing a condensed 
	 *  representation of electronic data (message). When a message of any length 
	 *  less than 2^64 bits (for SHA-1, SHA-224, and SHA-256) or less than 2^128 (for SHA-384 and SHA-512) 
	 *  is input to a hash algorithm, the result is an output called a message digest. 
	 *  A message digest ranges in length from 160 to 512 bits, depending on the algorithm.
	 *  
	 * @see http://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#MessageDigest
	 * @param algo
	 * @param orig
	 * @return
	 */
	public static String hash(String algo, String orig) {
		MessageDigest md = getInstance(algo);
		md.update(orig.getBytes());
		return Hasher.toHexString(md.digest());
	}

	/**
	 * Hash string to MD5 Algo
	 * @param orig
	 * @return
	 */
	public static String toMD5(String orig) {
		return hash("MD5", orig);
	}
	/**
	 * Hash string to SHA1 Algo
	 * @param orig
	 * @return
	 */
	public static String toSHA1(String orig) {
		return hash("SHA-1", orig);
	}

}
