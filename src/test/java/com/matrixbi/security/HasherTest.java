/********************************************************************
 * 
 * Copyrights (c) 2017 Matrix BI LTD to Present
 * All rights reserved.
 * 
 * @author Yehuda Korotkin <yehudako@matrixbi.co.il>
 *  
 ********************************************************************/
package com.matrixbi.security;

import java.util.HashMap;
import java.util.Map.Entry;

import org.testng.Assert;
import org.testng.annotations.*;

public class HasherTest {
	 
 @Test
 public void test_hashs() {
	 
	 /**
	  * Map of hashing string "TEST" to algos value
	  */
	 HashMap<String, String> TEST_hashes = new HashMap<String, String>();
	 TEST_hashes.put("MD2", "a96df14f8b5ccb567eede45bdddf189f");
	 TEST_hashes.put("MD5", "033bd94b1168d7e4f0d644c3c95e35bf");
	 TEST_hashes.put("SHA-1", "984816fd329622876e14907634264e6f332e9fb3");
	 TEST_hashes.put("SHA-224", "917ecca24f3e6ceaf52375d8083381f1f80a21e6e49fbadc40afeb8e");
	 TEST_hashes.put("SHA-256", "94ee059335e587e501cc4bf90613e0814f00a7b08bc7c648fd865a2af6a22cc2");
	 TEST_hashes.put("SHA-384", "4f37c49c0024445f91977dbc47bd4da9c4de8d173d03379ee19c2bb15435c2c7e624ea42f7cc1689961cb7aca50c7d17");
	 TEST_hashes.put("SHA-512", "7bfa95a688924c47c7d22381f20cc926f524beacb13f84e203d4bd8cb6ba2fce81c57a5f059bf3d509926487bde925b3bcee0635e4f7baeba054e5dba696b2bf");
	 
	 /**
	  * Check all hashes
	  */
	 for(Entry<String, String> x : TEST_hashes.entrySet())
		 Assert.assertEquals(x.getValue(), Hasher.hash(x.getKey(), "TEST"));
	 
	 /**
	  * Check function
	  */
	 Assert.assertEquals(Hasher.toMD5("TEST"), TEST_hashes.get("MD5"));
	 
 }

}